from django.contrib import admin
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from alldata.views import create_race_dates, get_venue_dates, process_result, get_result, store_dates

urlpatterns = [
    path('racedate/', create_race_dates),
    path('getvenuedates/', get_venue_dates),
    path('process_result/', process_result),
    path('get_result/', get_result),
    path('store_dates/', store_dates),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
]