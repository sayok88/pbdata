import json

from django.db import models

# Create your models here.
from alldata.constants import Venues


class RaceDates(models.Model):
    venue = models.IntegerField(choices=Venues.choices)
    race_date = models.DateField()

    class Meta:
        unique_together = ['venue', 'race_date']


class RaceResult(models.Model):
    race_date = models.ForeignKey(RaceDates, on_delete=models.DO_NOTHING)
    race_id = models.CharField(max_length=10)
    race_name = models.CharField(max_length=100)
    race_desc = models.CharField(max_length=100)
    race_length = models.CharField(max_length=10)
    race_time = models.CharField(max_length=10)
    winner_content = models.TextField()
    result = models.TextField()
    race_data1 = models.TextField()
    race_video = models.CharField(max_length=100)
    bets = models.TextField()
    racing_events = models.TextField()
    race_num = models.CharField(max_length=10)

    def get_data(self):
        fields = self._meta.concrete_fields
        skip = ['winner_content', 'result', 'bets', 'racing_events']
        data = {f.attname: getattr(self, f.attname) for f in fields if f.name not in skip}
        try:
            data['winner_content'] = json.loads(self.winner_content)
        except:
            data['winner_content'] = []
        try:
            data['result'] = json.loads(self.result)
        except:
            data['result'] = []
        try:
            data['bets'] = json.loads(self.bets)
        except:
            data['bets'] = []
        try:
            data['racing_events'] = json.loads(self.racing_events)
        except:
            data['racing_events'] = []
        return data

