# Generated by Django 3.2.6 on 2021-09-12 08:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('alldata', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RaceResult',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('race_id', models.CharField(max_length=10)),
                ('race_name', models.CharField(max_length=100)),
                ('race_desc', models.CharField(max_length=100)),
                ('race_length', models.CharField(max_length=10)),
                ('race_time', models.CharField(max_length=10)),
                ('winner_content', models.TextField()),
                ('result', models.TextField()),
                ('race_data1', models.TextField()),
                ('race_video', models.CharField(max_length=100)),
                ('bets', models.TextField()),
                ('racing_events', models.TextField()),
                ('race_date', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='alldata.racedates')),
            ],
        ),
    ]
