import datetime
import json

from django.db.models import CharField, F, Func, Value
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from rest_framework.authtoken.models import Token

from alldata.models import RaceDates, RaceResult

def verif_token(func):
    def inner(request, *args, **kwargs):
        if Token.objects.filter(key=request.GET.get('token')).exists():
            return func(request, *args, **kwargs)
        else:
            return JsonResponse({'error':'Not Authenticated'})
    return inner

@verif_token
def create_race_dates(request):
    venue = int(request.GET.get('venue'))
    date = datetime.datetime.strptime(request.GET.get('date'), '%Y-%m-%d').date()
    if not RaceDates.objects.filter(venue=venue, race_date=date).exists():
        r = RaceDates.objects.create(venue=venue, race_date=date)
        return JsonResponse({'id': r.id})
    return JsonResponse({'message': 'exists'})

@verif_token
def get_venue_dates(request):
    venue = int(request.GET.get('venue'))
    exclude_existing = request.GET.get('ee')

    if RaceDates.objects.filter(venue=venue).exists():
        if exclude_existing:
            existing = RaceResult.objects.filter(race_date__venue=venue).values_list('race_date__id', flat=True)
            print(len(existing))
            dates = [x.strftime('%Y-%m-%d') for x in
                     list(RaceDates.objects.filter(venue=venue).exclude(id__in=existing).order_by(
                         '-race_date').values_list('race_date', flat=True))]
        else:
            dates = [x.strftime('%Y-%m-%d') for x in
                 list(RaceDates.objects.filter(venue=venue).order_by('-race_date').values_list('race_date', flat=True))]
        return JsonResponse({'dates': dates})
    return JsonResponse({'message': 'Invalid venue'})

@verif_token
def process_result(request):
    if request.method == 'POST':

        data = json.loads(request.body)
        dt = datetime.datetime.strptime(data['date'], '%Y-%m-%d').date()
        rd = RaceDates.objects.filter(venue=data['venue'], race_date=dt)
        if rd:
            rd = rd.first()
        results = data['results']
        rr = RaceResult.objects.filter(race_date=rd)
        if len(rr)>0:
            return JsonResponse({'message': 'exists'})
        cnt = 0
        for result in results:
            proc_result = {k:v if isinstance(v, str) else json.dumps(v) for k,v in result.items() }
            proc_result['race_date'] = rd
            RaceResult.objects.create(**proc_result )
            cnt+=1
        return JsonResponse({'message': f"{cnt} data added"})
    return JsonResponse({'message': 'data'})

@verif_token
def get_result(request):
    venue = int(request.GET.get('venue'))
    date = datetime.datetime.strptime(request.GET.get('date'), '%Y-%m-%d').date()
    results = []
    if RaceDates.objects.filter(venue=venue, race_date=date).exists():
        rd = RaceDates.objects.filter(venue=venue, race_date=date).first()
        results = RaceResult.objects.filter(race_date=rd)
    return JsonResponse({'results':[r.get_data() for r in results]})


@verif_token
def store_dates(request):
    ids = []

    if request.method == 'POST':
        # print(request.body)
        # print(request.POST.get('dates'))
        data = json.loads(request.body)

        venue = int(request.GET.get('venue'))
        dates = data['dates']
        for date in dates:
            date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
            if not RaceDates.objects.filter(venue=venue, race_date=date).exists():
                r = RaceDates.objects.create(venue=venue, race_date=date)
                ids.append(r.id)
    return JsonResponse({'message':ids})