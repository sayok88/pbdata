from django.db import models


class Venues(models.IntegerChoices):
    MYSORE = 8
    HYDERABAD = 11
    KOLKATA = 1
    PUNE = 10
    OOTY_CHENNAI = 9
    DELHI = 7
    CHENNAI = 4
    MUMBAI = 2
    BANGALORE = 3